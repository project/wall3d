Drupal Wall 3d module 1.x:
---------------------------------
Original Author: eli .at. elimuir .dot. com
Requires - Drupal 6
License - GPL (see LICENSE)

Overview:
---------
Integrates the Wall 3D flash application into Drupal allowing images attached to nodes to be displayed in a 3D Flash environment.

Source code for Wall 3D is available from http://github.com/elimuir/3dWallofCool.git


Installation and configuration:
------------------------------
 1) Copy the wall3d folder into your 'modules' directory.
 2) Go to 'administer >> modules' to enable the "Wall3d" module.
 3) Go to 'administer >> access control' for allow users to watch the links.
 4) Use Views plugin to generate a feed url.  Copy the url for this feed for the next step.
 5) Go to 'administrator >> modules >> Wall 3d settings' and paste the url into the 'datafile:' parameter of the 'Flash Settings' field.


Last updated:
------------
